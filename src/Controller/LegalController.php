<?php
/**
 * Eurofurence Help Center
 * Legal Controller
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    draconigen@gmail.com
 * @link      https://www.eurofurence.org/help/legal
 * @since     0.3
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\ORM\TableRegistry;

class LegalController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->Auth->allow();
    }
    public function index() {
        return $this->redirect(['action' => 'imprint']);
    }

    public function imprint() {
        $this->set('text', $this->getText('imprint'));
    }

    public function privacy() {
        $this->set('text', $this->getText('privacy_statement'));
    }

    public function roc() {
        $this->set('text', $this->getText('rules_of_conduct'));
    }

    public function terms() {
        $this->set('text', $this->getText('terms_and_conditions'));
    }

    public function liability() {
        $this->set('text', $this->getText('liability_waiver'));
    }

    public function statutes() {
        $this->set('text', $this->getText('statutes_of_eurofurence_ev'));
    }

    

    public function attributions() {}
}


