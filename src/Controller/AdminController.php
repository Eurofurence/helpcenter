<?php
/**
 * Eurofurence Help Center
 * Authentication Controller
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    draconigen@gmail.com
 * @link      https://www.eurofurence.org/help/auth
 * @since     1.0
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Component\AuthComponent;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\I18n\Time;

class AdminController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function index() {
        $usersCtx = TableRegistry::get('users');
        $articlesCtx = TableRegistry::get('articles');
        $departmentsCtx = TableRegistry::get('departments');
        $templatesCtx = TableRegistry::get('templates');
        $textsCtx = TableRegistry::get('texts');

        $user = $this->Auth->user();

        $userDepartments = $this->getDepartmentsArray();
        $userDepartmentsIdArray = $this->getDepartmentsIdArray();

        $this->set('userDepartments', $userDepartments);
        $this->set('userRole', $user['role']);
        $this->set('userName', $user['username']);
        
        if (!empty($userDepartmentsIdArray)) {
            $this->set('userArticlesCount', $articlesCtx->find('all')
            ->where(['department_id' => $userDepartmentsIdArray], ['department_id' => 'integer[]'])
            ->count());
            
            $this->set('userDisabledArticlesCount', $articlesCtx->find('all')
            ->where(['enabled' => false, 'department_id' => $userDepartmentsIdArray], ['department_id' => 'integer[]'])
            ->count());

            $this->set('userTemplatesCount', $templatesCtx->find('all')
            ->where(['department_id' => $userDepartmentsIdArray], ['department_id' => 'integer[]'])
            ->count());
        }
        else {
            $this->set('userArticlesCount', 0);
            $this->set('userDisabledArticlesCount', 0);
            $this->set('userTemplatesCount', 0);
        }

        $this->set('articlesUrl', Router::url(['action' => 'articles'], true));
        $this->set('disabledArticlesUrl', Router::url(['action' => 'articles', 'disabled'], true));
        $this->set('templatesUrl', Router::url(['action' => 'templates'], true));
        $this->set('editUserUrl', Router::url(['controller' => 'auth', 'action' => 'edit'], true));
        $this->set('editDepartmentsUrl', Router::url(['controller' => 'admin', 'action' => 'departments'], true));
        
        // admin stuff below
        if ($user['role'] !== 'admin')
            return;

        $this->set('globalDepartmentsCount', $departmentsCtx->find('all')->count());
        $this->set('globalUsersCount', $usersCtx->find('all')->count());
        $this->set('globalDisabledUsersCount', $usersCtx->find('all')->where(['enabled' => false])->count());
        $this->set('globalArticlesCount', $articlesCtx->find('all')->count());
        $this->set('globalDisabledArticlesCount', $articlesCtx->find('all')->where(['enabled' => false])->count());
        $this->set('globalTemplatesCount', $templatesCtx->find('all')->count());
        $this->set('globalTextsCount', $textsCtx->find('all')->count());

        $this->set('departmentsUrl', Router::url(['action' => 'departments'], true));
        $this->set('usersUrl', Router::url(['controller' => 'auth', 'action' => 'users'], true));
        $this->set('textsUrl', Router::url(['controller' => 'admin', 'action' => 'texts'], true));
    }

    public function departments() {
        $departmentsCtx = TableRegistry::get('departments');

        $departmentsIdArray = $this->getDepartmentsIdArray($this->Auth->user('id'), $this->Auth->user('role') === 'admin');

        if ($this->request->is(['post', 'put'])) {
            $department = $departmentsCtx->newEntity($this->request->getData());

            if ($departmentsCtx->save($department)) {
                $this->Flash->success('Department saved.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error('Update failed!');
            }
        }
        
        if (empty($departmentsIdArray)) {
            $this->Flash->error('No departments assigned; please contact director or admin.');
            return $this->redirect($this->referer());
        }

        $departments = $departmentsCtx->find('all', ['contain' => 'users'])->where(['id' => $departmentsIdArray], ['id' => 'integer[]'])->toArray();

        if ($this->Auth->user('role') === 'admin') {
            $new = $departmentsCtx->newEntity();
            array_unshift($departments, $new);
        }

        $this->set('departments', $departments);
        $this->set('deleteUrl', Router::url(['controller' => 'admin', 'action' => 'delete', 'department'], true));
        $this->set('admin', $this->Auth->user('role') === 'admin');
    }

    public function templates() {
        $templatesCtx = TableRegistry::get('templates');
        $departmentsCtx = TableRegistry::get('departments');

        $departmentsIdArray = $this->getDepartmentsIdArray($this->Auth->user('id'), true);

        if ($this->request->is(['post', 'put'])) {
            $template = $templatesCtx->newEntity($this->request->getData());
            $template->department_id = $this->request->getData('Departments');

            if ($templatesCtx->save($template)) {
                $this->Flash->success('Template saved.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error('Update failed!');
            }
        }

        if (empty($departmentsIdArray)) {
            $this->Flash->error('No departments assigned; please contact director or admin.');
            return $this->redirect($this->referer());
        }

        $templates = $templatesCtx
            ->find('all', ['contain' => ['Departments']])
            ->where(['department_id' => $departmentsIdArray], ['department_id' => 'integer[]'])
            ->toArray();

        $new = $templatesCtx->newEntity();
        $new->department = $departmentsCtx->newEntity();

        array_unshift($templates, $new);

        $userDepartments = $this->getDepartmentsIdArray($this->Auth->user('id'), false);
        if (!empty($userDepartments))
            $this->set('firstUserDepartmentId', $userDepartments[0]);
        else
            $this->set('firstUserDepartmentId', 0);

        if (count($userDepartments) > 1)
            $this->set('showDepartmentPrefix', true);
        else
            $this->set('showDepartmentPrefix', false);

        $this->set('templates', $templates);
        $this->set('departments', ($departmentsCtx
            ->find('list', ['keyField' => 'id', 'valueField' => 'name']))
            ->where(['id' => $this->getDepartmentsIdArray($this->Auth->user('id'), true)], ['id' => 'integer[]'])
            ->toArray());
        $this->set('deleteUrl', Router::url(['controller' => 'admin', 'action' => 'delete', 'template'], true));
    }

    public function articles($show = 'all') {
        $articlesCtx = TableRegistry::get('articles');
        $departmentsCtx = TableRegistry::get('departments');

        $departmentsIdArray = $this->getDepartmentsIdArray($this->Auth->user('id'), true);

        if ($this->request->is(['post', 'put'])) {
            $article = $articlesCtx->newEntity($this->request->getData());
            $article->department_id = $this->request->getData('Departments');

            if ($articlesCtx->save($article)) {
                $this->Flash->success('Article saved.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error('Update failed!');
            }
        }

        if (empty($departmentsIdArray)) {
            $this->Flash->error('No departments assigned; please contact director or admin.');
            return $this->redirect($this->referer());
        }

        $conditions = ['department_id' => $departmentsIdArray];

        if ($show === 'disabled')
            array_unshift($conditions, ['enabled' => false]);
            
        $articles = $articlesCtx
            ->find('all', ['contain' => ['Departments']])
            ->where($conditions, ['department_id' => 'integer[]'])
            ->toArray();

        $new = $articlesCtx->newEntity();
        $new->department = $departmentsCtx->newEntity();

        array_unshift($articles, $new);
        
        $this->set('articles', $articles);
        $this->set('departments', $departments = ($departmentsCtx->find('list', ['keyField' => 'id', 'valueField' => 'name']))->toArray());
        

        $userDepartments = $this->getDepartmentsIdArray($this->Auth->user('id'), false);
        if (!empty($userDepartments))
            $this->set('firstUserDepartmentId', $userDepartments[0]);
        else
            $this->set('firstUserDepartmentId', 0);

        
        if (count($userDepartments) > 1 || $this->Auth->user('role') === 'admin')
            $this->set('showDepartmentPrefix', true);
        else
            $this->set('showDepartmentPrefix', false);

        if ($show !== 'disabled') {
            $this->set('switchViewText', 'Show disabled and suggestions only');
            $this->set('switchViewUrl', Router::url(['action' => 'articles', 'disabled']));
        }
        else {
            $this->set('switchViewText', 'Show All');
            $this->set('switchViewUrl', Router::url(['action' => 'articles', 'all']));
        }
            
        $this->set('deleteUrl', Router::url(['controller' => 'admin', 'action' => 'delete', 'article'], true));
    }

    function texts() {
        if ($this->Auth->user('role') !== 'admin') {
            $this->Flash->error('Access denied.');
            return $this->redirect(['controller' => 'admin', 'action' => 'index']);
        }

        $textsCtx = TableRegistry::get('texts');
        $usersCtx = TableRegistry::get('users');

        if ($this->request->is(['post', 'put'])) {
            $text = $textsCtx->newEntity($this->request->getData());
            $text->user_id = $this->Auth->user('id');

            if ($textsCtx->save($text)) {
                $this->Flash->success('Text saved.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error('Update failed!');
            }
        }

        $texts = $textsCtx->find('all', ['contain' => ['users']])->toArray();

        $new =  $textsCtx->newEntity();
        $new->user_id = $usersCtx->newEntity();
        array_unshift($texts, $new);
        
        $this->set('texts', $texts);
        $this->set('deleteUrl', Router::url(['controller' => 'admin', 'action' => 'delete', 'text'], true));
        $this->set('users', $usersCtx->find('list', ['keyField' => 'id', 'valueField' => 'username'])->toArray());
    }

    /*
     *  Function to delete Articles, Departments, Suggestions and Templates
     *  Required to be called by the following URL pattern: /admin/delete/:context/:id/:token,
     *  whereas :context is (in well-formed url notation) either 'article', 'department', 'suggestion' or 'template',
     *  :id is the id of the record to be deleted and
     *  :token must have been written into the session prior calling the function under the key "DeleteTokens.$Contexts.$id".
     *  Example: $this->session()->write('DeleteTokens.Articles.' . $id, $token); // $token should be some unique alphanum gibberish.
     */
    public function delete($context, $id, $token) {
        // transform url-typed context string to conform entity notation
        $context = ucfirst($context) . 's';

        $tableCtx = TableRegistry::get($context);

        // check if the user has been given permission to delete this
        if ($this->request->session()->consume('DeleteTokens.' . $context . '.' . $id) === $token) {
            if ($tableCtx) {
                $entity = $tableCtx->get($id);
                try{
                    if ($entity && $tableCtx->delete($entity)) {
                        $this->Flash->success('Deleted.');
                    }
                    // Unexpected: Failed to delete record. Id invalid?
                    else
                        $this->Flash->error('Failed to delete entity.');
                }
                catch(\PDOException $e) {
                    $this->Flash->error('PDOException caught - Possible Integrity Constraint Violation?');
                }
            }
            // Unexpected: TableRegistry failed to find the according table. Malformed context string?
            else
                $this->Flash->error('Failed to retrieve entity context');
        }
        // No corresponding token found under the expected session key, deny permission.
        else
            $this->Flash->error('Permission denied.');

        return $this->redirect($this->referer());
    }

    private function getDepartmentsArray($userId = null) {
        if ($userId === null)
            $userId = $this->Auth->user('id');

        $departmentsCtx = TableRegistry::get('departments');

        $this->request->session()->write('Temp.subjectId', $userId);

        $userDepartments = $departmentsCtx->find('all'/*, ['fields' => ['id', 'name']]*/)
        ->matching('Users', 
        function ($q) {
            return $q->where(['Users.id' => $this->request->session()->consume('Temp.subjectId')]);
        })->toArray();

        return $userDepartments;
    }

    private function getDepartmentsIdArray($userId = null, $allDepartments = false) {
        if ($userId === null)
            $userId = $this->Auth->user('id');

        if ($allDepartments && $this->Auth->user('role') === 'admin') {
            $departmentsCtx = TableRegistry::get('departments');
            $departmentsIdArray = [];
            $departmentsIdOrmArray = $departmentsCtx->find('all', ['fields' => ['id']])->toArray();
            foreach($departmentsIdOrmArray as $orm)
                $departmentsIdArray[] = $orm->id;
            return $departmentsIdArray;
        }

        $userDepartments = $this->getDepartmentsArray($userId);

        $userDepartmentsIdArray = [];
        foreach($userDepartments as $ud) {
            $userDepartmentsIdArray[] = $ud->id;
        }

        return $userDepartmentsIdArray;
    }
}