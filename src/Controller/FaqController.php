<?php
/**
 * Eurofurence Help Center
 * FAQ Controller
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    draconigen@gmail.com
 * @link      https://www.eurofurence.org/help/faq
 * @since     0.1
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Event\Event;
// use Cake\Filesystem\File; // for manual debug



class FaqController extends AppController
{
    public $paginate = [
        'limit' => 20,
        'order' => [
            'articles.question' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->Auth->allow();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Paginator');
    }

    public function beforeFilter(Event $event) {   
        // disable CSRF for repeated ajax search queries
        $this->Security->setConfig('unlockedActions', ['search']);
    }

    public function index() {
        
        $departmentsCtx = TableRegistry::get('Departments');
        $articlesCtx = TableRegistry::get('Articles');

        $departments = $departmentsCtx->find('list', ['keyField' => 'id', 'valueField' => 'name']);
        
        $this->set('departments', $departments);
        $this->set('target', Router::url(['controller' => 'faq', 'action' => 'search'], true));
        $this->set('permalink', Router::url(['controller' => 'faq', 'action' => 'view'], true));
    }

    public function search(/* int $page : pagination happens automagically */) {
        return $this->ftsearch();       // own SQL Fulltext Search
        // return $this->c48ftsearch(); // SQL Fulltext Search Plugin by chris48
    }

    public function view($id = null) {
        if ($id == null)
            return $this->redirect(['action' => 'index']);

        $articlesCtx = TableRegistry::get('Articles');
        $departmentsCtx = TableRegistry::get('Departments');
        
        $data = $articlesCtx->get($id, [
            'contain' => ['Departments']
        ]);

        $data->question = $this->filterArticleString($data->question);
        $data->answer = $this->filterArticleString($data->answer);

        $this->set('data', $data->toArray());

        $this->set('departmentUrl', Router::url(['controller' => 'contact', 'action' => 'index', $data->department->reference], true));
    }

    public function suggest() {
        $departmentsCtx = TableRegistry::get('Departments');
        $articlesCtx = TableRegistry::get('Articles');

        $departments = ($departmentsCtx->find('list', ['keyField' => 'id', 'valueField' => 'name']))->toArray();
        
        $departments = [0 => '(Please choose one)'] + $departments;

        $this->set('department_id', $departments);
        $this->set('gSitekey', Configure::read('GoogleReCaptcha.sitekey'));

        /* after "SUBMIT" has been pressed */
        if ($this->request->is('post')) {
            $department_id  = $this->request->getData('department_id');
            $question       = $this->request->getData('question');
            $reCaptcha      = $this->request->getData('g-recaptcha-response');
            
            try {
                $department = $departmentsCtx->get($department_id);
            }
            catch(\Exception $e) {
                // This case is handled later, in verifyInput().   
            }

            if ($this->verifyInput($department_id, $question, $reCaptcha)) {
                $this->request->data('enabled', 0);

                $new = $articlesCtx->newEntity($this->request->getData());

                if (!$articlesCtx->save($new)) {
                    $this->Flash->error('Sorry, something went wrong. Please try again later or contact website support.');
                    return $this->redirect($this->referer());
                }

                if ($department) {
                    $subject = '[EFHC] New FAQ Suggestion';
                    $articlesUrl = Router::url(['controller' => 'admin', 'action' => 'articles', 'disabled'], true);
                    $message = "Dear " . $department->name . " team,\n\nYou have received a new FAQ suggestion:\n\n$question\n\nTo accept and answer or decline the suggestion, go to\n\n$articlesUrl\n\nBest regards,\nThe Eurofurence Help Center";

                    Email::deliver($department->email, $subject, $message, 'admin');
                }

                $this->Flash->modal('Thank you! Your suggestion has been sent to the given department for consideration. Please note that it might take us some time to process your idea.');
            }
            else {
                return;
            }
        }
    }

    private function verifyInput($department, $question, $reCaptcha) {
        $error = false;

        if ($reCaptcha === null || !$this->verifyReCaptcha($reCaptcha)) {
            $error = true;
            $this->Flash->error('ReCaptcha verification failed. Please try again.');
        }

        if (empty($department)) {
            $error = true;
            $this->Flash->error('Please choose a department.');
        }

        if (strlen($question) < 10) {
            $error = true;
            $this->Flash->error('Please enter a question.');
        }
        
        return !$error;
    }

    private function verifyReCaptcha($response) {
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret={{secret}}&response={{response}}';
            
        $url = str_replace('{{secret}}', Configure::read('GoogleReCaptcha.secret'), $url);
        $url = str_replace('{{response}}', $response, $url);
        
        $result = file_get_contents($url);
        $resultData = json_decode($result);

        return $resultData->success;
    }

    private function ftsearch() {

        $this->autoRender = false;

        if (!$this->request->is('ajax'))
			return $this->redirect(['action' => 'index']);

        $articlesCtx = TableRegistry::get('Articles');

        // Core SQL Query building

        $r = $this->request->getData();
        $q = $r['Question'];
        $a = $r['Answer'];
        $search = $r['Search'];
        $conditions = [];

        $search = $this->filterFtSearchString($search);

        $conditions['department_id'] = $r['Departments'];          // WHERE department_id IN (...)
        $conditions['enabled'] = true;

    	// search based on a search string
        if (!empty($search) && $search != '*') {

            switch([$q, $a]) {

                // Question AND Answer
                case [1,1] :
                    $conditions[] = "MATCH(question, answer) AGAINST (:search IN BOOLEAN MODE)";
                    break;

                // Question only
                case [1,0] :
                    $conditions[] = "MATCH(question) AGAINST (:search IN BOOLEAN MODE)";
                    break;

                // Answer only
                case [0,1] :
                    $conditions[] = "MATCH(answer) AGAINST (:search IN BOOLEAN MODE)";
                    break;

                // Neither Question nor Answer selected
                default :
                    $conditions[] = "MATCH(question, answer) AGAINST (:search IN BOOLEAN MODE)";
            }

            $results = $articlesCtx->find('all', ['fields' => ['id', 'question', 'answer']])
            ->where($conditions, [
                'department_id' => 'integer[]' // required type declaration for department_id
            ])
            ->bind(':search', $search);
        }

        // empty search string
        else {
            $results = $articlesCtx->find('all', ['fields' => ['id', 'question', 'answer']])
            ->where($conditions, [
                'department_id' => 'integer[]'
            ]);
        }

        $results = $this->paginate($results);

        /* debug */
		//  $f = new File('debug_dump.txt');
		//  $f->write(print_r($search, true));
		//  $f = new File('debug_dump_2.txt');
		//  $f->write(print_r($results, true));
		/* debug */

        $paginationData = [
            'page'      => $this->request->getParam('paging')['Articles']['page'],
            'count'     => $this->request->getParam('paging')['Articles']['count'],
            'pageCount' => $this->request->getParam('paging')['Articles']['pageCount'],
            'hasPrev'   => $this->request->getParam('paging')['Articles']['prevPage'],
            'hasNext'   => $this->request->getParam('paging')['Articles']['nextPage']
        ];
        
        $return['data'] = $this->filterResults($results->toArray());

        $return['pagination'] = $paginationData;

        if (Configure::read('debug')) {
            $return['query'] = $search;
        }

        return $this->response->withType('application/json')
        ->withStringBody(
            json_encode($return)
        );
    }

    private function c48ftsearch() {
        // source & documentation: https://github.com/chris48s/cakephp-searchable

        /*  to use this...
            - uncomment the plugin loading line in /config/bootstrap.php:216
            - uncomment the Behaviour line in /src/Model/Table/ArticlesTable.php:12
        */

        $this->autoRender = false;

        if (!$this->request->is('ajax'))
			return $this->redirect(['action' => 'index']);

        $departmentsCtx = TableRegistry::get('Departments');
        $articlesCtx = TableRegistry::get('Articles');

        // Core SQL Query building

        $r = $this->request->getData();
        $q = $r['Question'];
        $a = $r['Answer'];
        $search = $r['Search'];
        $conditions = [];

        $search = $this->filterFtSearchString($search);       

        $conditions['department_id'] = $r['Departments'];          // WHERE department_id IN (...)

    	// search based on a search string

        switch([$q, $a]) {
            // Question only
            case [1,0] : $match = 'question'; break;

            // Answer only
            case [0,1] : $match = 'answer'; break;

            default : $match = 'question, answer';
        }

        $results = $articlesCtx->find('matches', [
            [
                'match' => $match,
                'against' => $search,
                'mode' => 'IN BOOLEAN MODE'
            ]
        ])
        ->where($conditions, [
            'department_id' => 'integer[]' // required type declaration for department_id
        ]);
        
        return $this->response->withType('application/json')
        ->withStringBody(json_encode($results));
    }

    private function filterFtSearchString($str) {
        /*  The following regex is supposed to filter anything but alphanumeric characters at the beginning and end of a
            search string as well as removing all most special characters from within the string.
            Apparently, MySQL's Full Text Search treats the occurances of a list of ascii characters in a special way,
            despite them being bound and used within quotes.
            On this topic: https://stackoverflow.com/a/25972465/5410292 
        */

        // return preg_replace('/(^[^\w]+|[^\w\*\+\-\.\,\:\;\!\?\/\\€\$\%\&\#]|[^\w\*]+$|(\*){2,}$)/',' ', $str);
       
        return preg_replace('/(^[^\w]+|[^\w\*\+\-]|[^\w\*]+$|(\*){2,}$)/',' ', $str);
    }

    private function filterResults($articles) {
        $ret = [];
        foreach($articles as $a) {
            $ret[] = $this->filterArticleString($a->toArray());
        }
        return $ret;
    }

    private function filterArticleString($str) {
        return preg_replace('/\s\#[a-zA-Z0-9][^\;]+/', '', $str);
    }
}
