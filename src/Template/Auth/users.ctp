<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');


echo '<h1>Backend User Control</h1>';

echo '<ul uk-accordion>';

foreach($users as $u) {
	echo '<li>';
	echo '<h3 class="uk-accordion-title'.((!$u->enabled || $u->token)? ' efhc-red' : '').'">' . h($u->username) . '</h3>';
	echo '<div class="uk-accordion-content">';
	echo $this->Form->create($u, ['id' => 'efhc-faqSearchForm']);
	echo $this->Form->control('id');
	echo $this->Form->control('username');
	echo $this->Form->control('email');
	echo $this->Form->control('enabled');
	echo $this->Form->control('role', [
		'type' => 'select',
		'options' => ['user' => 'User', 'admin' => 'Admin']
	]);
	
	$userDepartments = [];
	foreach($u->departments as $d) {
		$userDepartments[] = $d->id;
	}
	
	echo $this->Form->input('departmentsList', [
		'label' => 'Departments',
		'type' => 'select', 
		'multiple' => 'checkbox', 
		'options' => $departmentsList,
		'value' => $userDepartments
	]);

	echo '<div class="uk-margin-bottom"></div>';

	echo $this->Form->control('token');

	echo '<a href="' . $deleteUrl . '/' . $u->id . '" class="uk-button uk-button-danger efhc-float-right efhc-admin-delete" onclick="return confirm(\'Confirm deletion of user ' . h($u->username) . '\')"><span class="efhc-icon-position-fix" uk-icon="icon: trash"></span> DELETE</a>';

	echo $this->Form->control('created', ['disabled' => true]);
	echo $this->Form->control('modified', ['disabled' => true]);
	
	echo $this->Form->submit('Save', [
		'type' => 'submit',
		'value' => 'Submit',
		'class' => 'uk-button uk-margin-top efhc-submit-button'
	]);
	echo $this->Form->end();
	echo '</div>';
	echo '</li>';
}

echo '</ul>'; // uk-acccordion

