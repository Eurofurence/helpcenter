<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');

echo '<h1>Registration</h1>';

echo $this->Form->create();

echo $this->Form->input('username', ['required' => true]);

echo $this->Form->input('passwd1', [
	'type' => 'password',
	'label' => 'New Password',
	'required' => true,
]);
echo $this->Form->input('passwd2', [
	'type' => 'password',
	'label' => 'Repeat new Password',
	'required' => true,
]);

echo $this->Form->input('email', ['required' => true, 'label' => 'eMail']);

echo $this->Form->submit('Submit', [
	'type' => 'submit',
	'value' => 'Submit',
	'class' => 'uk-button uk-margin-top efhc-submit-button'
]);

echo $this->Form->end();

?>

<div class="uk-card uk-card-default uk-margin-top">
	<div class="uk-card-body">
		<h3 class="uk-card-title">Please Note</h3>
		<p>You're about to register for the administration backend. This section is for directors and authorized staff personell only.<br />Every registration must be confirmed manually before allowed to log in.</p>
	</div>
</div>
