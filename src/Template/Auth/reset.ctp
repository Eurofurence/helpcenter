<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');

echo '<h1>Password Reset</h1>';

echo '<p>Please provide the eMail address that you originally used upon registration. If you cannot remember this, please contact the website administration.</p>';

echo $this->Form->create();

echo $this->Form->input('email');

echo $this->Form->submit('Submit', [
	'type' => 'submit',
	'value' => 'Submit',
	'class' => 'uk-button uk-margin-top efhc-submit-button'
]);

echo $this->Form->end();
