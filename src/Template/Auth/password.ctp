<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');

echo '<h1>Password Reset</h1>';

echo $this->Form->create($user);

echo '<p>Please enter your new password.</p>';

echo $this->Form->control('email', ['disabled' => true]);
echo $this->Form->input('passwd1', [
	'type' => 'password',
	'label' => 'New Password',
	'required' => true,
]);
echo $this->Form->input('passwd2', [
	'type' => 'password',
	'label' => 'Repeat new Password',
	'required' => true,
]);


echo $this->Form->submit('Submit', [
	'type' => 'submit',
	'value' => 'Submit',
	'class' => 'uk-button uk-margin-top efhc-submit-button'
]);

echo $this->Form->end();
