<?php

use Cake\Routing\Router;

$this->layout = 'base-light';
$this->assign('title', 'Backend');
?>

<h1>eMail Templates</h1>

<?php

echo '<ul uk-accordion' . (count($templates) > 1? '>' : '="active: 0">');

foreach($templates as $t) {
	echo '<li>';
	echo '<h3 class="uk-accordion-title">' . ($showDepartmentPrefix && $t->department->name ? '[' . h($t->department->name) . '] ' : '') . ($t->subject? h($t->subject) : '<span class="efhc-icon-position-fix uk-text-muted" uk-icon="icon: plus-circle"></span> <span class="uk-text-muted">New Template</span>') . '</h3>';
	echo '<div class="uk-accordion-content">';
	echo $this->Form->create($t);
	echo $this->Form->control('id');
	echo $this->Form->control('reference', ['required' => true, 'label' => 'Url Slug']);
	echo $this->Form->control('subject', ['required' => true]);
	echo $this->Form->control('message', ['required' => true]);

	if ($t->department->id === null) {
		$t->department->id = $firstUserDepartmentId;
	}
	echo $this->Form->input('Departments', [
		'type' => 'select', 
		'options' => $departments,
		'default' => $t->department->id,
		'label' => 'Assigned Department',
		//'onchange' => 'UIkit.notification("Warning: be aware that assigning another department than your own will move this template out of your reach.", {status: "warning", timeout : 15000})'
	]);
	
	if ($t->id !== null) {
		$deleteToken = md5(uniqid(rand(), true));
		$this->request->session()->write('DeleteTokens.Templates.' . $t->id, $deleteToken);
		echo '<a href="' . $deleteUrl . '/' . $t->id . '/' . $deleteToken . '" class="uk-button uk-button-danger efhc-float-right efhc-admin-delete" onclick="return confirm(\'You are about to permanently delete this template. Proceed?\')"><span class="efhc-icon-position-fix" uk-icon="icon: trash"></span> DELETE</a>';

		echo $this->Form->control('created', ['disabled' => true]);
		echo $this->Form->control('modified', ['disabled' => true]);

		$url = Router::url(['controller' => 'contact', 'action' => 'index', $t->department->reference, h($t->reference)], true);

		echo '<p><strong>Public Link: </strong><a href="' . $url . '" target="_blank">' . $url . '</a></p>';
			
	}


	echo $this->Form->submit('Save', [
		'type' => 'submit',
		'value' => 'Submit',
		'class' => 'uk-button uk-margin-top efhc-submit-button',
	]);
	echo $this->Form->end();
	echo '</div>';
	echo '</li>';
}

echo '</ul>'; // uk-accordion