<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');
?>

<h1>Website Texts</h1>

<?php

echo '<ul uk-accordion' . (count($texts) > 1? '>' : '="active: 0">');

foreach($texts as $t) {
	echo '<li>';
	echo '<h3 class="uk-accordion-title'.((!$t->enabled)? ' efhc-red' : '').'">' . ($t->reference? h($t->reference) : '<span class="efhc-icon-position-fix uk-text-muted" uk-icon="icon: plus-circle"></span> <span class="uk-text-muted">New Article</span>') . '</h3>';
	echo '<div class="uk-accordion-content">';
	echo $this->Form->create($t);
	echo $this->Form->control('id');
	echo $this->Form->control('reference', ['required' => true]);
	echo $this->Form->control('enabled');
	echo $this->Form->control('notes', ['label' => 'Internal Notes']);
	echo $this->Form->control('content');
	
	if ($t->id !== null) {
		$deleteToken = md5(uniqid(rand(), true));
		$this->request->session()->write('DeleteTokens.Texts.' . $t->id, $deleteToken);
		echo '<a href="' . $deleteUrl . '/' . $t->id . '/' . $deleteToken . '" class="uk-button uk-button-danger efhc-float-right efhc-admin-delete" onclick="return confirm(\'You are about to permanently delete this text. Proceed?\')"><span class="efhc-icon-position-fix" uk-icon="icon: trash"></span> DELETE</a>';

		echo $this->Form->control('created', ['disabled' => true]);
		echo $this->Form->control('modified', ['disabled' => true]);
		echo 'Last edit by: ' . h($users[$t->user_id]);
	}

	echo $this->Form->submit('Save', [
		'type' => 'submit',
		'value' => 'Submit',
		'class' => 'uk-button uk-margin-top efhc-submit-button',
	]);
	echo $this->Form->end();
	echo '</div>';
	echo '</li>';
}

echo '</ul>'; // uk-accordion