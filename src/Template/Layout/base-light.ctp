<?php
use Cake\Routing\Router;

/**
 * Eurofurence Help Center
 * Base Light Layout
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    draconigen@gmail.com
 * @link      https://www.eurofurence.org/help/
 * @since     0.1
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Eurofurence Help Center:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('uikit.min.css') ?>
    <?= $this->Html->css('efhelp_basewhite.css') ?>
    
    <?= $this->Html->script('jquery-3.2.1.min.js') ?>
    <?= $this->Html->script('uikit.min.js') ?>
    <?= $this->Html->script('uikit-icons.min.js') ?>
    <?= $this->Html->script('efhc-functions.js') ?>
    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <header>
        <div class="efhc-container">
            <h1>Eurofurence Help Center &ndash; <?= $this->fetch('title') ?></h1>
        </div>
    </header>
    <?php
        $act['contact'] = $act['faq'] = $act['legal'] = $act['admin'] = '';

        switch([strtolower($this->request->controller)]) {
            case ['contact']: $act['contact'] = 'efhc-active'; break;
            case ['faq']    : $act['faq'] = 'efhc-active'; break;
            case ['legal']  : $act['legal'] = 'efhc-active'; break;
            case ['admin']  : $act['admin'] = 'efhc-active'; break;
        }
    ?>
    <nav role="navigation">
        <ul>
            <li><a href="<?= Router::url(['controller' => 'contact', 'action' => 'index'], true) ?>" class="<?= $act['contact'] ?>">Contact</a></li>
            
            <li><a href="<?= Router::url(['controller' => 'faq', 'action' => 'index'], true) ?>" class="<?= $act['faq'] ?>">FAQ<span uk-icon="icon:triangle-down"></span></a>
                <ul>
                    <li><a href="<?= Router::url(['controller' => 'faq', 'action' => 'index'], true) ?>" class="<?= $act['faq'] ?>">Questions &amp; Answers</a></li>
                    <li><a href="<?= Router::url(['controller' => 'faq', 'action' => 'suggest'], true) ?>" class="<?= $act['faq'] ?>">Suggest a Question</a></li>
                </ul>
            </li>

            <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'index'], true) ?>" class="<?= $act['legal'] ?>">Legal<span uk-icon="icon:triangle-down"></span></a>
                <ul>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'imprint'], true) ?>" class="<?= $act['legal'] ?>">Imprint</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'privacy'], true) ?>" class="<?= $act['legal'] ?>">Privacy Statement</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'roc'], true) ?>" class="<?= $act['legal'] ?>">Rules of Conduct</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'terms'], true) ?>" class="<?= $act['legal'] ?>">Terms and Conditions</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'liability'], true) ?>" class="<?= $act['legal'] ?>">Liability Waiver</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'statutes'], true) ?>" class="<?= $act['legal'] ?>">Statutes of Eurofurence e.V.</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'attributions'], true) ?>" class="<?= $act['legal'] ?>">Site Attributions</a></li>
                </ul>
            </li>

            <?php if ($this->request->session()->read('Auth.User.id')) { ?>
                <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'index'], true) ?>" class="<?= $act['admin'] ?>">Administration<span uk-icon="icon:triangle-down"></span></a>
                    <ul>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'index'], true) ?>" class="<?= $act['admin'] ?>">Overview</a></li>
                        <li><a href="<?= Router::url(['controller' => 'auth', 'action' => 'edit'], true) ?>" class="<?= $act['admin'] ?>">Your Account</a></li>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'departments'], true) ?>" class="<?= $act['admin'] ?>">Departments</a></li>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'templates'], true) ?>" class="<?= $act['admin'] ?>">eMail Templates</a></li>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'articles'], true) ?>" class="<?= $act['admin'] ?>">FAQ Articles</a></li>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'articles', 'disabled'], true) ?>" class="<?= $act['admin'] ?>">Disabled FAQ &amp; Suggestions</a></li>
                        <?php if ($this->request->session()->read('Auth.User.role') === 'admin') { ?>
                            <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'texts'], true) ?>" class="<?= $act['admin'] ?>">Website Texts</a></li>
                            <li><a href="<?= Router::url(['controller' => 'auth', 'action' => 'users'], true) ?>" class="<?= $act['admin'] ?>">Edit Users</a></li>
                        <?php } ?>
                        <li><a href="<?= Router::url(['controller' => 'auth', 'action' => 'logout'], true) ?>">Logout</a></li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </nav>

    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer class="efhc-light">
        <div class="efhc-container">
            <div>
                <h3>Contact us:</h3>
                <ul class="uk-list">
                    <li class="efhc-light"><a href="<?= Router::url(['controller' => 'contact','action' => 'index'], true) ?>" class="efhc-contact">Contact Center</a></li>
                    <li class="efhc-light"><a href="<?= Router::url(['controller' => 'contact','action' => 'index', 'web'], true) ?>" class="efhc-contact">Website Support</a></li>
                </ul>
            </div>
            <div>
                <h3>Find us on:</h3>
                <div class="uk-button-group">
                    <a target="_blank" href="https://www.eurofurence.org/" class="uk-icon-button" uk-icon="icon:home"></a> 
                    <a target="_blank" href="https://www.twitter.com/eurofurence" class="uk-icon-button" uk-icon="icon:twitter"></a> 
                    <a target="_blank" href="https://www.facebook.com/eurofurence" class="uk-icon-button" uk-icon="icon:facebook"></a> 
                    <a target="_blank" href="https://plus.google.com/+EurofurenceOrgOfficial" class="uk-icon-button" uk-icon="icon:google-plus"></a>
                    <a target="_blank" href="https://vimeo.com/eurofurence" class="uk-icon-button" uk-icon="icon:vimeo"></a>
                </div>
            </div>
            <div>
                <h3>Legal:</h3>
                <ul class="uk-list">
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'imprint'], true) ?>"><span uk-icon="icon:bookmark"></span>Imprint &amp; Legal Notice</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'attributions'], true) ?>"><span uk-icon="icon:heart" class="efhc-big-icon-fix"></span>Site Attributions</a></li>
                    <!-- <li>&nbsp;</li> -->
                </ul>
            </div>
        </div> <!-- .container -->
    </footer>

    <script>beatifyNotifications(); </script>
    <?php include('ef-cookienotice.html'); ?>
</body>
</html>
