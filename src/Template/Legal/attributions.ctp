<?php

$this->layout = 'base-light';
$this->assign('title', 'Attributions');

echo '<div class="uk-card uk-card-default">';

echo '<div class="uk-card-body">';
echo '<h3 class="uk-card-title">Third Party Attributions</h3>';
echo '<p>';
echo 'The Eurofurence Help Center was built with the help of the following projects:';
// echo '<a href="https://www.cakephp.org/" target="_blank"><span uk-icon="icon:sign-out"></span>CakePHP</a> and ';
// echo '<a href="https://wwww.getuikit.com/" target="_blank"><span uk-icon="icon:sign-out"></span>UIkit</a>';
echo '</p>';

echo '<div class="efhc-cards">';

echo '<div class="uk-card uk-card-default uk-card-body uk-card-hover">';
echo '<a href="https://www.cakephp.org/" target="_blank" />';
echo $this->Html->image('cakephp-logo.gif', ['alt' => 'CakePHP']);
echo '</a>';
echo '</div>'; // .uk-card

echo ' '; // makes the word-spacing work

echo '<div class="uk-card uk-card-default uk-card-body uk-card-hover">';
echo '<a href="https://www.getuikit.com/" target="_blank" />';
echo $this->Html->image('uikit-logo.gif', ['alt' => 'UIkit']);
echo '</a>';
echo '</div>'; // .uk-card

// echo ' '; // makes the word-spacing work

// echo '<div class="uk-card uk-card-default uk-card-body uk-card-hover">';
// echo '<a href="https://jquery.com/" target="_blank" />';
// echo $this->Html->image('jquery-logo.gif', ['alt' => 'jQuery']);
// echo '</a>';
// echo '</div>'; // .uk-card

echo '</div>'; // .efhc-cards

echo '</div>'; // .uk-card-body

echo '<div class="uk-card-footer">';

echo '<p><span uk-icon="icon:heart" class="efhc-big-icon-fix"></span>Thank you!</p>';

echo '</div>'; // .uk-card-footer
echo '</div>'; // .uk-card .uk-card-default



echo '<div class="uk-card uk-card-default uk-margin-top">';

echo '<div class="uk-card-body">';
echo '<h3 class="uk-card-title">Eurofurence Help Center Repository</h3>';
echo '<p>';
echo 'If you wish to obtain a copy of this software for your own purposes or review its\' code, visit our repository:';
echo '</p>';

echo '<div class="efhc-cards">';

echo '<div class="uk-card uk-card-default uk-card-body uk-card-hover">';
echo '<a href="https://bitbucket.org/Eurofurence/helpcenter" target="_blank" />';
echo $this->Html->image('bitbucket-logo.gif', ['alt' => 'BitBucket']);
echo '</a>';
echo '</div>'; // .uk-card

echo '</div>'; // .efhc-cards

echo '</div>'; // .uk-card-body

echo '</div>'; // .uk-card .uk-card-default