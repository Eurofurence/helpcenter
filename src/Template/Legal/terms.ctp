<?php
$this->layout = 'base-light';
$this->assign('title', 'Terms and Conditions');

if ($text['modified']) {
	$modified = $text['modified']->format('d.m.Y');
}
else {
	$modified = false;
}

?>

<div class="uk-card uk-card-default">
	<div class="uk-card-body">
		
	<?php 
	if (!empty($text['content'])) {
		echo $text['content'];
	}
	else {
		echo '<div class="efhc-missing-content-notice">';
		echo '<h3><span class="efhc-icon-position-fix" uk-icon="icon: warning"></span> Content Unavailable</h3>';
		echo '<p>We\'re sorry, it seems this page is currently undergoing maintenance and should be back online in a moment.<br />If this status persists, or if you believe this is in error, please notify our Website Support. Thank you!</p>';
		echo '</div>';
	}
	?>

	</div> <!-- .uk-card-body -->

	<?php 
	if ($modified) {
		echo '<div class="uk-card-footer">';
		echo '<p class="uk-text-small">Updated: '. $modified . '</p>'; 
		echo '</div>';
	} 
	?>

</div> <!-- .uk-card -->
