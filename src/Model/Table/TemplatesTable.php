<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class TemplatesTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Departments');
        $this->addBehavior('Timestamp');
    }
}