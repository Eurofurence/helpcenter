<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class DepartmentsTable extends Table
{
    public function initialize(array $config)
    {
        $this->hasMany('Articles');
        $this->hasMany('Suggestions');
        $this->hasMany('Templates');
        $this->belongsToMany('Users');
        $this->addBehavior('Timestamp');
    }
}