<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class DepartmentsUsersTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->primaryKey(['user_id', 'department_id']);
    }
}