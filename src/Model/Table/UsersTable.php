<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsToMany('Departments');
        $this->hasMany('Texts');
        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('username', 'Please check your username input.')    
            ->notEmpty('password', 'Please provide a password.')
            ->notEmpty('email', 'Please check your password input.');
        return $validator;
    }
}