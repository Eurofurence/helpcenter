/* beatify Cake's flash notifications */
function beatifyNotifications() {
	// error messages
	$.each($('div.message.error.efhc-uk-notification-error'), function(i, div) {
		$(div).hide(0);
		UIkit.notification({
			'message': '<span uk-icon="icon: warning" class="efhc-icon-lift"></span> ' + $(div).text(),
			'status' : 'danger',
			'timeout': 0
		});
	})

	// success messages
	$.each($('div.message.success.efhc-uk-notification-success'), function(i, div) {
		$(div).hide(0);
		UIkit.notification({
			'message': '<span uk-icon="icon: check" class="efhc-icon-lift"></span> ' + $(div).text(),
			'status' : 'success',
			'timeout': 10000
		});
	})

	// default messages
	$.each($('div.message.default.efhc-uk-notification-default'), function(i, div) {
		$(div).hide(0);
		UIkit.notification({
			'message': '<span uk-icon="icon: info" class="efhc-icon-lift"></span> ' + $(div).text(),
			'status' : 'primary',
			'timeout': 10000
		});
	})

	// modal messages
	$.each($('div.message.efhc-uk-notification-modal'), function(i, div) {
		$(div).hide(0);
		UIkit.modal.alert('<span uk-icon="icon: info"></span> ' + $(div).text());
	})
}